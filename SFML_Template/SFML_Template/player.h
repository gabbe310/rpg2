
#pragma once
#include <SFML/Graphics.hpp>

class player
{	

public:
	//Constructor & deConstructor
	player();
	~player();

	void render(sf::RenderTarget* window);

private:

	sf::Sprite Player_Sprite;
	sf::Texture Player_Texture;

	void PlayerChar();
	void PlayerMove();
	void Create_Sprite(sf::Sprite P_SP, sf::Texture	P_TX);
};



