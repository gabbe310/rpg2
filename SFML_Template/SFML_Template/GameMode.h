#pragma once
#include <SFML/Graphics.hpp>
#include 
class GameMode
{
public:
	GameMode();
	~GameMode();

	void init();
	void update(sf::RenderWindow & win);
	void render(sf::RenderWindow & win);

	Map GameMap;
};

